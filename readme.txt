Triple field drupal module
---------------------------
ACKNOWLEDGEMENTS:
The code for this module is based on the double_field module 
http://drupal.org/project/double_field 
developed by Chi 
http://drupal.org/user/556138

SUMMARY:

  This is a small module written to provide extensions to Drupal's core Text Fields.
  By this module you can divide your text fields into three separate parts.
  Example: key/value/value, first name/last name/department, speaker name/institution/title etc.

INSTALLATION:

  1. Copy the module folder to your server.
  2. Enable the module via the modules page.
  see http://drupal.org/node/895232 for further information


CONFIGURATION:


  * Go to admin/structure/types to add or edit triple fields for your content types.


MAINTAINERS:

  * knowledges33ker - http://drupal.org/user/164847/
  